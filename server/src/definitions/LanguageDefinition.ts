/*
	Copyright (C) Paul Hocker. All rights reserved.
	Licensed under the MIT License. See License.txt in the project root for license information.
*/

// Interfaces

export interface IParameter {
	name: string;
	type: "string" | "value" | "enum" | "label";
	values?: string[];
}

export interface IPreProcessor {
	name: string;
	description: string;
	example: string;
	parameters: IParameter[];
}

export interface IInstruction {
	name: string;
	description: string;
	group: string;
	parameters: IParameter[];
}

export interface IPseudoOp {
	name: string;
	otherNames: string[];
	canHaveLabel: boolean;
	description: string;
	documentation: string[];
	parameters: IParameter[];
	relatedTo: string[];
}

export interface ILanguageDefinition {
	Instructions: IInstruction[];
	PseudoOps: IPseudoOp[];
	Extensions: {[key: string]:string[]};
	PreProcessor: IPreProcessor[];
	Directives: IDirective[];
}

export interface IDirective {
	name: string;
	insertLabel: string;
	otherNames: string[];
	description: string;
	documentation: string[];
	parameters: IParameter[];
	relatedTo: string[];
}

export const Extensions = {
	source: [
		".kick",
		".asm",
		".a",
	],
	include: [
		".h",
	],
};

export const PreProcessor:IPreProcessor[] = [
	{
		name: "#define",
		description: "Defines a preprocessor symbol.",
		example: "\#define DEBUG",
		parameters : [
			{
				name: "symbol",
				type: "string",
			}
		]
	},
	{
		name: "#elif",
		description: "The combination of an \#else\ and an \#if\ preprocessor directive.",
		example: "",
		parameters : [
			{
				name: "symbol",
				type: "string",
			}
		]
	},
	{
		name: "#else",
		description: "Used after an \#if\ to start an else block which is executed if the condition is false.",
		example: "",
		parameters: []
	},
	{
		name: "#endif",
		description: "Marks the end of an #if/#else block.",
		example: "",
		parameters: []
	},
	{
		name: "#if",
		description: "Discards the sourcecode after the \#if\-directive if the condition is false.",
		example: "",
		parameters : [
			{
				name: "symbol",
				type: "string",
			}
		]
	},
	{
		name: "#import",
		description: "Imports another sourcefile.",
		example: "",
		parameters : [
			{
				name: "name",
				type: "string",
			}
		]
	},
	{
		name: "#importif",
		description: "Imports another sourcefile if the given expression is evaluated to true.",
		example: "",
		parameters : [
			{
				name: "symbol",
				type: "string",
			},
			{
				name: "filename",
				type: "string"
			}
		]
	},
	{
		name: "#importonce",
		description: "Make the assembler skip the current file if it has already been imported.",
		example: "",
		parameters: []
	},
	{
		name: "#undef",
		description: "	Removes the definition of a preprocessor symbol.",
		example: "",
		parameters : [
			{
				name: "symbol",
				type: "string",
			}
		]
	}
];

export const Instructions:IInstruction[] = [
	{
		name: "ADC",
		description: "ADd to accumulator with Carry",
		group: "Arithmetic",
		parameters: [],
	},
	{
		name: "AND",
		description: "AND memory with accumulator",
		group: "Logical",
		parameters: [],
	},
	{
		name: "ASL",
		description: "Accumulator Shift Left",
		group: "Shift and Rotate",
		parameters: [],
	},
	{
		name: "BCC",
		description: "Branch on Carry Clear (C = 0)",
		group: "Jump, Branch, Compare, and Test",
		parameters: [],
	},
	{
		name: "BCS",
		description: "Branch on Carry Set (C = 1)",
		group: "Jump, Branch, Compare, and Test",
		parameters: [],
	},
	{
		name: "BEQ",
		description: "Branch on EQual to zero (Z = 1)",
		group: "Jump, Branch, Compare, and Test",
		parameters: [],
	},
	{
		name: "BIT",
		description: "test BITs",
		group: "Jump, Branch, Compare, and Test",
		parameters: [],
	},
	{
		name: "BMI",
		description: "Branch on MInus (N = 1)",
		group: "Jump, Branch, Compare, and Test",
		parameters: [],
	},
	{
		name: "BNE",
		description: "Branch on Not Equal to zero (Z = 0)",
		group: "Jump, Branch, Compare, and Test",
		parameters: [],
	},
	{
		name: "BPL",
		description: "Branch on PLus (N = 0)",
		group: "Jump, Branch, Compare, and Test",
		parameters: [],
	},
	{
		name: "BRK",
		description: "BReaK",
		group: "Other",
		parameters: [],
	},
	{
		name: "BVC",
		description: "Branch on oVerflow Clear (V = 0)",
		group: "Jump, Branch, Compare, and Test",
		parameters: [],
	},
	{
		name: "BVS",
		description: "Branch on oVerflow Set (V = 1)",
		group: "Jump, Branch, Compare, and Test",
		parameters: [],
	},
	{
		name: "CLC",
		description: "CLear Carry flag",
		group: "Set and Reset (Clear)",
		parameters: [],
	},
	{
		name: "CLD",
		description: "CLear Decimal mode",
		group: "Set and Reset (Clear)",
		parameters: [],
	},
	{
		name: "CLI",
		description: "CLear Interrupt disable",
		group: "Set and Reset (Clear)",
		parameters: [],
	},
	{
		name: "CLV",
		description: "CLear oVerflow flag",
		group: "Set and Reset (Clear)",
		parameters: [],
	},
	{
		name: "CMP",
		description: "CoMPare memory and accumulator",
		group: "Jump, Branch, Compare, and Test",
		parameters: [],
	},
	{
		name: "CPX",
		description: "ComPare memory and X",
		group: "Jump, Branch, Compare, and Test",
		parameters: [],
	},
	{
		name: "CPY",
		description: "ComPare memory and Y",
		group: "Jump, Branch, Compare, and Test",
		parameters: [],
	},
	{
		name: "DEC",
		description: "DECrement memory by one",
		group: "Increment and Decrement",
		parameters: [],
	},
	{
		name: "DEX",
		description: "DEcrement X by one",
		group: "Increment and Decrement",
		parameters: [],
	},
	{
		name: "DEY",
		description: "DEcrement Y by one",
		group: "Increment and Decrement",
		parameters: [],
	},
	{
		name: "EOR",
		description: "Exclusive-OR memory with Accumulator",
		group: "Logical",
		parameters: [],
	},
	{
		name: "INC",
		description: "INCrement memory by one",
		group: "Increment and Decrement",
		parameters: [],
	},
	{
		name: "INX",
		description: "INcrement X by one",
		group: "Increment and Decrement",
		parameters: [],
	},
	{
		name: "INY",
		description: "INcrement Y by one",
		group: "Increment and Decrement",
		parameters: [],
	},
	{
		name: "JMP",
		description: "JuMP to another location (GOTO)",
		group: "Jump, Branch, Compare, and Test",
		parameters: [],
	},
	{
		name: "JSR",
		description: "Jump to SubRoutine",
		group: "Subroutine",
		parameters: [],
	},
	{
		name: "LDA",
		description: "LoaD the Accumulator",
		group: "Load and Store",
		parameters: [],
	},
	{
		name: "LDX",
		description: "LoaD the X register",
		group: "Load and Store",
		parameters: [],
	},
	{
		name: "LDY",
		description: "LoaD the Y register",
		group: "Load and Store",
		parameters: [],
	},
	{
		name: "LSR",
		description: "Logical Shift Right",
		group: "Shift and Rotate",
		parameters: [],
	},
	{
		name: "NOP",
		description: "No OPeration",
		group: "Other",
		parameters: [],
	},
	{
		name: "ORA",
		description: "OR memory with Accumulator",
		group: "Logical",
		parameters: [],
	},
	{
		name: "PHA",
		description: "PusH Accumulator on stack",
		group: "Transfer",
		parameters: [],
	},
	{
		name: "PHP",
		description: "PusH Processor status on stack",
		group: "Transfer",
		parameters: [],
	},
	{
		name: "PLA",
		description: "PulL Accumulator from stack",
		group: "Transfer",
		parameters: [],
	},
	{
		name: "PLP",
		description: "PulL Processor status from stack",
		group: "Transfer",
		parameters: [],
	},
	{
		name: "ROL",
		description: "ROtate Left",
		group: "Shift and Rotate",
		parameters: [],
	},
	{
		name: "ROR",
		description: "ROtate Right",
		group: "Shift and Rotate",
		parameters: [],
	},
	{
		name: "RTI",
		description: "ReTurn from Interrupt",
		group: "Subroutine",
		parameters: [],
	},
	{
		name: "RTS",
		description: "ReTurn from Subroutine",
		group: "Subroutine",
		parameters: [],
	},
	{
		name: "SBC",
		description: "SuBtract from accumulator with Carry",
		group: "Arithmetic",
		parameters: [],
	},
	{
		name: "SEC",
		description: "SEt Carry",
		group: "Set and Reset (Clear)",
		parameters: [],
	},
	{
		name: "SED",
		description: "SEt Decimal mode",
		group: "Set and Reset (Clear)",
		parameters: [],
	},
	{
		name: "SEI",
		description: "SEt Interrupt disable",
		group: "Set and Reset (Clear)",
		parameters: [],
	},
	{
		name: "STA",
		description: "STore the Accumulator",
		group: "Load and Store",
		parameters: [],
	},
	{
		name: "STX",
		description: "STore the X register",
		group: "Load and Store",
		parameters: [],
	},
	{
		name: "STY",
		description: "STore the Y register",
		group: "Load and Store",
		parameters: [],
	},
	{
		name: "TAX",
		description: "Transfer Accumulator to X",
		group: "Transfer",
		parameters: [],
	},
	{
		name: "TAY",
		description: "Transfer Accumulator to Y",
		group: "Transfer",
		parameters: [],
	},
	{
		name: "TSX",
		description: "Transfer Stack pointer to X",
		group: "Stack",
		parameters: [],
	},
	{
		name: "TXA",
		description: "Transfer X to accumulator",
		group: "Transfer",
		parameters: [],
	},
	{
		name: "TXS",
		description: "Transfer X to Stack pointer",
		group: "Stack",
		parameters: [],
	},
	{
		name: "TYA",
		description: "Transfer Y to Accumulator",
		group: "Transfer",
		parameters: [],
	},
];

export const PseudoOps:IPseudoOp[] = [
	{
		name: "PROCESSOR",
		otherNames: [],
		canHaveLabel: false,
		description: "Sets the processor model",
		documentation: [
			"Determines byte order and integer formats for the assembled data.",
			"Can only be executed once, and should be the first thing encountered by the assembler.",
		],
		parameters: [
			{
				name: "type",
				type: "enum",
				values: [ "6502", "6803", "HD6303", "68705", "68HC11", "F8" ],
			},
		],
		relatedTo: [],
	}
];

export const Directives:IDirective[] = [
	{
		name: "*",
		insertLabel: "",
		description: "",
		documentation: [],
		otherNames: [],
		relatedTo: [],
		parameters: []
	},
	{
		name: ".align",
		insertLabel: "",
		description: "",
		documentation: [],
		otherNames: [],
		relatedTo: [],
		parameters: []
	},
	{
		name: ".assert",
		insertLabel: "",
		description: "",
		documentation: [],
		otherNames: [],
		relatedTo: [],
		parameters: []
	},
	{
		name: ".asserterror",
		insertLabel: "",
		description: "",
		documentation: [],
		otherNames: [],
		relatedTo: [],
		parameters: []
	},
	{
		name: ".by",
		insertLabel: "",
		description: "",
		documentation: [],
		otherNames: [],
		relatedTo: [],
		parameters: []
	},
	{
		name: ".byte",
		insertLabel: "",
		description: "",
		documentation: [],
		otherNames: [],
		relatedTo: [],
		parameters: []
	},
	{
		name: ".const",
		insertLabel: "",
		description: "",
		documentation: [],
		otherNames: [],
		relatedTo: [],
		parameters: []
	},
	{
		name: ".define",
		insertLabel: "",
		description: "",
		documentation: [],
		otherNames: [],
		relatedTo: [],
		parameters: []
	},
	{
		name: ".disk",
		insertLabel: "",
		description: "",
		documentation: [],
		otherNames: [],
		relatedTo: [],
		parameters: []
	},
	{
		name: ".dw",
		insertLabel: "",
		description: "",
		documentation: [],
		otherNames: [],
		relatedTo: [],
		parameters: []
	},
	{
		name: ".dword",
		insertLabel: "",
		description: "",
		documentation: [],
		otherNames: [],
		relatedTo: [],
		parameters: []
	},
	{
		name: ".encoding",
		insertLabel: "",
		description: "",
		documentation: [],
		otherNames: [],
		relatedTo: [],
		parameters: []
	},
	{
		name: ".enum",
		insertLabel: "",
		description: "",
		documentation: [],
		otherNames: [],
		relatedTo: [],
		parameters: []
	},
	{
		name: ".error",
		insertLabel: "",
		description: "",
		documentation: [],
		otherNames: [],
		relatedTo: [],
		parameters: []
	},
	{
		name: ".errorif",
		insertLabel: "",
		description: "",
		documentation: [],
		otherNames: [],
		relatedTo: [],
		parameters: []
	},
	{
		name: ".eval",
		insertLabel: "",
		description: "",
		documentation: [],
		otherNames: [],
		relatedTo: [],
		parameters: []
	},
	{
		name: ".file",
		insertLabel: "",
		description: "",
		documentation: [],
		otherNames: [],
		relatedTo: [],
		parameters: []
	},
	{
		name: ".filemodify",
		insertLabel: "",
		description: "",
		documentation: [],
		otherNames: [],
		relatedTo: [],
		parameters: []
	},
	{
		name: ".filenamespace",
		insertLabel: "",
		description: "",
		documentation: [],
		otherNames: [],
		relatedTo: [],
		parameters: []
	},
	{
		name: ".fill",
		insertLabel: "",
		description: "",
		documentation: [],
		otherNames: [],
		relatedTo: [],
		parameters: []
	},
	{
		name: ".for",
		insertLabel: "",
		description: "",
		documentation: [],
		otherNames: [],
		relatedTo: [],
		parameters: []
	},
	{
		name: ".if",
		insertLabel: "",
		description: "",
		documentation: [],
		otherNames: [],
		relatedTo: [],
		parameters: []
	},
	{
		name: ".import binary",
		insertLabel: "",
		description: "",
		documentation: [],
		otherNames: [],
		relatedTo: [],
		parameters: []
	},
	{
		name: ".import c64",
		insertLabel: "",
		description: "",
		documentation: [],
		otherNames: [],
		relatedTo: [],
		parameters: []
	},
	{
		name: ".import source",
		insertLabel: "",
		description: "",
		documentation: [],
		otherNames: [],
		relatedTo: [],
		parameters: []
	},
	{
		name: ".import text",
		insertLabel: "",
		description: "",
		documentation: [],
		otherNames: [],
		relatedTo: [],
		parameters: []
	},
	{
		name: ".importonce",
		insertLabel: "",
		description: "",
		documentation: [],
		otherNames: [],
		relatedTo: [],
		parameters: []
	},
	{
		name: ".macro",
		insertLabel: "",
		description: "",
		documentation: [],
		otherNames: [],
		relatedTo: [],
		parameters: []
	},
	{
		name: ".memblock",
		insertLabel: "",
		description: "",
		documentation: [],
		otherNames: [],
		relatedTo: [],
		parameters: []
	},
	{
		name: ".modify",
		insertLabel: "",
		description: "",
		documentation: [],
		otherNames: [],
		relatedTo: [],
		parameters: []
	},
	{
		name: ".namespace",
		insertLabel: "",
		description: "",
		documentation: [],
		otherNames: [],
		relatedTo: [],
		parameters: []
	},
	{
		name: ".pc",
		insertLabel: "",
		description: "",
		documentation: [],
		otherNames: [],
		relatedTo: [],
		parameters: []
	},
	{
		name: ".plugin",
		insertLabel: "",
		description: "",
		documentation: [],
		otherNames: [],
		relatedTo: [],
		parameters: []
	},
	{
		name: ".print",
		insertLabel: "",
		description: "",
		documentation: [],
		otherNames: [],
		relatedTo: [],
		parameters: []
	},
	{
		name: ".printnow",
		insertLabel: "",
		description: "",
		documentation: [],
		otherNames: [],
		relatedTo: [],
		parameters: []
	},
	{
		name: ".pseudocommand",
		insertLabel: "",
		description: "",
		documentation: [],
		otherNames: [],
		relatedTo: [],
		parameters: []
	},
	{
		name: ".pseudopc",
		insertLabel: "",
		description: "",
		documentation: [],
		otherNames: [],
		relatedTo: [],
		parameters: []
	},
	{
		name: ".return",
		insertLabel: "",
		description: "",
		documentation: [],
		otherNames: [],
		relatedTo: [],
		parameters: []
	},
	{
		name: ".segment",
		insertLabel: "",
		description: "",
		documentation: [],
		otherNames: [],
		relatedTo: [],
		parameters: []
	},
	{
		name: ".segmentdef",
		insertLabel: "",
		description: "",
		documentation: [],
		otherNames: [],
		relatedTo: [],
		parameters: []
	},
	{
		name: ".struct",
		insertLabel: "",
		description: "",
		documentation: [],
		otherNames: [],
		relatedTo: [],
		parameters: []
	},
	{
		name: ".te",
		insertLabel: "",
		description: "",
		documentation: [],
		otherNames: [],
		relatedTo: [],
		parameters: []
	},
	{
		name: ".text",
		insertLabel: "",
		description: "",
		documentation: [],
		otherNames: [],
		relatedTo: [],
		parameters: []
	},
	{
		name: ".var",
		insertLabel: "",
		description: "",
		documentation: [],
		otherNames: [],
		relatedTo: [],
		parameters: []
	},
	{
		name: ".while",
		insertLabel: "",
		description: "",
		documentation: [],
		otherNames: [],
		relatedTo: [],
		parameters: []
	},
	{
		name: ".wo",
		insertLabel: "",
		description: "",
		documentation: [],
		otherNames: [],
		relatedTo: [],
		parameters: []
	},
	{
		name: ".word",
		insertLabel: "",
		description: "",
		documentation: [],
		otherNames: [],
		relatedTo: [],
		parameters: []
	},
	{
		name: ".zp",
		insertLabel: "",
		description: "",
		documentation: [],
		otherNames: [],
		relatedTo: [],
		parameters: []
	},
	{
		name: ".label",
		insertLabel: "",
		description: "Assigns a given expression to a label.",
		documentation: [],
		otherNames: [],
		relatedTo: [],
		parameters: [
			{
				name: "expression",
				type: "string"
			}
		]
	},
	{
		name: ".function",
		insertLabel: "function",
		description: "Defines a function.",
		documentation: [],
		otherNames: [],
		relatedTo: [],
		parameters: [
			{
				name: "expression",
				type: "string"
			}
		]
	}
];

export const LanguageDefinition:ILanguageDefinition = {
	Instructions,
	PseudoOps,
	Extensions,
	PreProcessor,
	Directives,
};

export default LanguageDefinition;

