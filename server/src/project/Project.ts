/*
	Copyright (C) Paul Hocker. All rights reserved.
	Licensed under the MIT License. See License.txt in the project root for license information.
    
    Project.ts
  
    Represents the Currently Opened Source File
  
    In Kick Assembler each Source FIle is treated as its
    own independant Project.
  
    In the future, the definition of a Project may change
    to allow for things like making disks, compressing, etc.
  
*/

import Logging from "../utils/Logging";
const log = Logging.log();

import {
    TextDocument, CompletionItemKind, TextDocumentIdentifier,
} from "vscode-languageserver";

import { Assembler, IAssemblerResult } from "./../providers/Assembler";
import { KickAssembler, IAssemblerResults, ILine, ISyntax, ISourceRange, IFile } from "../assembler/KickAssembler";
import { ISettings } from "../providers/SettingsProvider";

import PathUtils from "../utils/PathUtils";
import StringUtils from "../utils/StringUtils";
import LineUtils from "../utils/LineUtils";
import SourceUtils from "../utils/SourceUtils";

import {
	writeFileSync,
	unlinkSync,
	readFileSync,
} from 'fs';

export enum SymbolTypes {
	Label,
	Constant,
	Function,
	Macro,
	PseudoCommand,
	Variable,
	Namespace
}

export interface ISymbol {
    type: SymbolTypes;
    name: string;
    detail:string;
	value: string;
	kind: CompletionItemKind;
    line: ILine;
    scope: number;
    isExternal: boolean;
    isGlobal: boolean;
};


export default class Project {

    private _assemblerResults?:IAssemblerResult;
    private _kickAssemblerResults?:IAssemblerResults
    private _sourceLines?:string[];
    private _lines?:ILine[];
    private _assembler:Assembler;
    private _kickAssembler:KickAssembler;
    private _symbols:ISymbol[];

    constructor() {
        log.trace("[Project]");
        this._assembler = new Assembler();
        this._kickAssembler = new KickAssembler();
        this._assemblerResults = undefined;
        this._kickAssemblerResults = undefined;
        this._symbols = undefined;
    }

    public getAssemblerResults():IAssemblerResult|undefined {
        log.trace("[Project] getAssemblerResults");
        return this._assemblerResults;
    }

    public getNewAssemblerResults():IAssemblerResults|undefined {
        log.trace("[Project] getNewAssemblerResults");
        return this._kickAssemblerResults;
    }

    public getSourceLines():string[]|undefined {
        log.trace("[Project] getSourceLines");
        return this._sourceLines;
    } 

    public getLines(textDocument:TextDocumentIdentifier):ILine[]|undefined {
        log.trace("[Project] getLines");
		var pathname = PathUtils.uriToPlatformPath(PathUtils.uriToPlatformPath(textDocument.uri));
        var data = readFileSync(pathname, 'utf8');
        var lines = StringUtils.splitIntoLines(data);
        var file:IFile;
        return SourceUtils.getLines(lines, file);
    }

    public getSymbols():ISymbol[]|undefined {
        log.trace("[Project] getSymbols");
        if (!this._symbols) {
            this._symbols = this.createSymbols(this._kickAssemblerResults);
        }
        return this._symbols;
    }

    public assemble(settings:ISettings, document:TextDocument, processIncludes:boolean|false) {
        
        log.trace("[Project] assemble");
        //this._assemblerResults = this._assembler.assemble(settings, document);
        var start = process.hrtime();
        this._kickAssemblerResults = this._kickAssembler.assemble(settings, document, false);
        this._sourceLines = StringUtils.splitIntoLines(document.getText());
        this._symbols = undefined;
        var stop = process.hrtime(start);
        log.trace(`assemble time ${stop[0]}`);
    }

    //public updateSource(settings:ISettings, document:TextDocument) {
    //    log.trace("[Project] updateSource");
    //    this._sourceLines = StringUtils.splitIntoLines(document.getText());
    //    this._lines = SourceUtils.getLines(this._sourceLines);
    //}

    private createSymbols(assemblerResults:IAssemblerResults):ISymbol[]|undefined {
        log.trace("[Project] createSymbols");
        var sortBy = require('sort-by')

        var symbols:ISymbol[] = [];

		//	find out which file index has the autoinclude
		let autoincludeFileIndex = 0;
		
		for(var i = 0; i < assemblerResults.sourceFiles.length; i++) {
			var sourceFile = assemblerResults.sourceFiles[i];
			if (sourceFile.file.system) {
				autoincludeFileIndex = sourceFile.file.index;
			}			
		}

        if (assemblerResults.assemblerInfo.syntax) {
        	for (let syntax of assemblerResults.assemblerInfo.syntax) {
                //  if not the kick assembler auto include
                if (syntax.sourceRange.fileIndex != autoincludeFileIndex) {
                    //  create a new symbol
                    var symbol = this.createSymbol(syntax, assemblerResults.sourceFiles[syntax.sourceRange.fileIndex].lines);
                    if (symbol) {
                        symbols.push(symbol);
                    }
                }
            }
        }

		//	sort by scope
		symbols.sort(sortBy('-scope'))
        return symbols;
    }

    private createSymbol(syntax:ISyntax, lines:ILine[]):ISymbol|undefined {

		var type = syntax.type.toLowerCase();
        var sourceRange = syntax.sourceRange;
        var line = lines[syntax.sourceRange.startLine];
        var text = line.text;
        
        var symbol:ISymbol;
        log.trace(`type:${type}, text:${text}, sourceRange:${sourceRange}`);
        
		if (type == "label") {
			symbol = this.createFromLabel(sourceRange, text);
		}

		if (type == "directive") {
			symbol = this.createFromDirective(sourceRange, text);
		}

        if (symbol) {
            symbol.isExternal = true;
            symbol.line = line;
            return symbol;
        }

		log.fine(`unhandled syntax type [${syntax.type}]`);
		return null;
    }
    
	private createFromLabel(sourceRange:ISourceRange, text:string):ISymbol {
        log.trace("[Project] createFromLabel");
        var name = text.substr(sourceRange.startPosition, (sourceRange.endPosition - 1) - sourceRange.startPosition);
        var symbol = <ISymbol>{};
        symbol.name = name;
        symbol.type = SymbolTypes.Label;
        symbol.kind = CompletionItemKind.Reference;
        symbol.scope = this._kickAssemblerResults.sourceFiles[sourceRange.fileIndex].lines[sourceRange.startLine].scope;
		return symbol;
	}

	private createFromDirective(sourceRange:ISourceRange, text:string):ISymbol {
        log.trace("[Project] createFromDirective");
        var name = text.substr(sourceRange.startPosition, sourceRange.endPosition - sourceRange.startPosition);
        log.trace("- type: " + name);

		if (name.toLowerCase() == ".var") {
            var symbol = this.createFromSimpleValue(text.substr(sourceRange.endPosition));
            symbol.kind = CompletionItemKind.Variable;
            symbol.type = SymbolTypes.Variable;
            symbol.scope = this._kickAssemblerResults.sourceFiles[sourceRange.fileIndex].lines[sourceRange.startLine].scope;
			return symbol;
		}

		if (name.toLowerCase() == ".label") {
			var symbol = this.createFromSimpleValue(text.substr(sourceRange.endPosition));
            symbol.kind = CompletionItemKind.Reference;
            symbol.type = SymbolTypes.Label;
            symbol.scope = this._kickAssemblerResults.sourceFiles[sourceRange.fileIndex].lines[sourceRange.startLine].scope;
			return symbol;
		}

		if (name.toLowerCase() == ".namespace") {
            let parms = text.split(" ");
            let name = parms[1].trim();
            var symbol = <ISymbol>{};
            symbol.name = name;
            symbol.kind = CompletionItemKind.Reference;
            symbol.type = SymbolTypes.Namespace;
            symbol.scope = this._kickAssemblerResults.sourceFiles[sourceRange.fileIndex].lines[sourceRange.startLine].scope;
			return symbol;
		}

		if (name.toLowerCase() == ".const") {
			var symbol = this.createFromSimpleValue(text.substr(sourceRange.endPosition));
            symbol.kind = CompletionItemKind.Value;
            symbol.type = SymbolTypes.Constant;
            symbol.scope = this._kickAssemblerResults.sourceFiles[sourceRange.fileIndex].lines[sourceRange.startLine].scope;
			return symbol;
		}

        
		if (name.toLowerCase() == ".function") {
			var split = StringUtils.splitFunction(text);
			if (split.length > 0) {
                var name = split[1];
                var symbol = <ISymbol>{};
                symbol.type = SymbolTypes.Function;
                symbol.kind = CompletionItemKind.Function;
                symbol.name = name;
                symbol.scope = this._kickAssemblerResults.sourceFiles[sourceRange.fileIndex].lines[sourceRange.startLine].scope;
                if (symbol.name.substr(0,1) == "@") {
                    symbol.scope = 0;
                    symbol.name = symbol.name.substr(1);
                    symbol.isGlobal = true;
                }
				return symbol;
			}
		}
        
		if (name.toLowerCase() == ".macro") {
			var split = StringUtils.splitFunction(text);
			if (split.length > 0) {
                var name = split[1];
                var symbol = <ISymbol>{};
                symbol.type = SymbolTypes.Macro;
                symbol.kind = CompletionItemKind.Snippet;
                symbol.name = name;
                symbol.scope = this._kickAssemblerResults.sourceFiles[sourceRange.fileIndex].lines[sourceRange.startLine].scope;
                if (symbol.name.substr(0,1) == "@") {
                    symbol.scope = 0;
                    symbol.name = symbol.name.substr(1);
                    symbol.isGlobal = true;
                }
				return symbol;
			}
		}
        
        
		if (name.toLowerCase() == ".pseudocommand") {
			var split = StringUtils.splitPseudoCommand(text);
			if (split.length > 0) {
                var name = split[0];
                var symbol = <ISymbol>{};
                symbol.type = SymbolTypes.PseudoCommand;
                symbol.kind = CompletionItemKind.Snippet;
                symbol.name = name;
                symbol.scope = this._kickAssemblerResults.sourceFiles[sourceRange.fileIndex].lines[sourceRange.startLine].scope;
                if (symbol.name.substr(0,1) == "@") {
                    symbol.scope = 0;
                    symbol.name = symbol.name.substr(1);
                    symbol.isGlobal = true;
                }
				return symbol;
			}
		}
        

		log.trace(`unhandled directive type [${name}]`);
	}

	//	a value is a simple x = y scenario
	private createFromSimpleValue(text:string):ISymbol {

        var symbol = <ISymbol>{};

        if (text.indexOf("=") >= 0) {
            let parms = text.split("=");
            let name = parms[0].trim();
            let value = parms[1].trim();
            symbol.name = name;
            symbol.value = value;
        } else {
            symbol.name = text.trim();
        }

		return symbol;
	}

}