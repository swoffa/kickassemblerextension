/*
	Copyright (C) Paul Hocker. All rights reserved.
	Licensed under the MIT License. See License.txt in the project root for license information.
*/

import * as vscode from 'vscode';
import * as fs from 'fs';

export default class ConfigUtils {

	public static validateSettings():boolean {
		let settings = vscode.workspace.getConfiguration("kickassembler");

		if (!fs.existsSync(settings.get("assemblerPath"))) {
			vscode.window.showErrorMessage("Invalid Assembler Path");
			return false;
		}
		
		if (!fs.existsSync(settings.get("javaPath"))) {
			vscode.window.showErrorMessage("Invalid Java Path");
			return false;
		}

        return true;
	}
}